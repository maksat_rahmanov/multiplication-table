export class MultiplicationItem {
  constructor(private _multiplier: number, private _factor: number) {
  }

  public get product(): number {
    return this._multiplier * this._factor;
  }

  public toString(): string {
    return `${this._multiplier.toString()} x ${this._factor.toString()} =`;
  }
}
