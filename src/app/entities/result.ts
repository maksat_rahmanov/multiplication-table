import { ResultStat } from "../util/result.stat";

export class ResultModel {
  private readonly _title: string;
  private readonly _percentage: number;
  private readonly _emoji: string;

  constructor(private _correctAnswers: number, private _totalAnswers: number) {
    const stat = ResultStat.getResultStat(_correctAnswers, _totalAnswers);

    this._title = stat.title;
    this._emoji = stat.emoji;
    this._percentage = stat.percentage;
  }

  public get title(): string {
    return this._title;
  }

  public get emoji(): string {
    return this._emoji;
  }

  public get percentage(): number {
    return Math.ceil(this._percentage);
  }

  public get incorrectAnswers(): number {
    return this._totalAnswers - this._correctAnswers;
  }
}
