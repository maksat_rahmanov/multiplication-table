import { expect } from "chai";
import { ResultStat } from "../util/result.stat";
import { IResultStat } from "../entities/IResultStat";

describe("Result stat (100%)", () => {
  let stat: IResultStat = null;

  beforeEach(() => {
    stat = ResultStat.getResultStat(20, 20);
  });

  it("should return percentage", () => {
    expect(stat.percentage).to.equal(100);
  });

  it("should return title", () => {
    expect(stat.title).to.equal("Отлично!");
  });

  it("should return emoji", () => {
    expect(stat.emoji).to.equal("&#x1F603;");
  });
});
