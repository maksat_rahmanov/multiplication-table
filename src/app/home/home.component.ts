import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import {MultiplicationItem} from '../entities/multiplication.item';
import {GameMode} from '../entities/game.mode';
import {ArrayUtil} from '../util/array.util';
import {ResultModel} from '../entities/result';
import {BehaviorSubject} from 'rxjs';

interface GameBoard {
  [mode: number]: number[];
}

interface GameLevel {
  label: string;
  mode: GameMode;
}

interface AnswerBoard {
  image: string;
  table: Array<MultiplicationItem[]>;
}

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit {
  currentQuestion$ = new BehaviorSubject<string>("");
  answerBoard$ = new BehaviorSubject<AnswerBoard>({ image: "", table: [] });
  mode: GameMode;
  resultModel: ResultModel;
  levels: GameLevel[] = [
    { label: "Легкая", mode: GameMode.Easy },
    { label: "Средняя", mode: GameMode.Normal },
    { label: "Высокая", mode: GameMode.Hard }
  ];

  @ViewChild("resultPopup", null) resultPopup: ElementRef;
  @ViewChild("startPopup", null) startPopup: ElementRef;

  private _correctAnswersCount: number;
  private _currentStep: number;
  private _questionsTable: MultiplicationItem[];
  private _multiplicationTable: Array<MultiplicationItem[]>;
  private readonly _classOpen = "open";
  private readonly _maxImages = 20; // images available in game

  private _gameBoard: GameBoard = {
    // board size per each game mode
    [GameMode.Easy]: [4, 5],
    [GameMode.Normal]: [5, 8],
    [GameMode.Hard]: [8, 10],
  };

  constructor(private cdref: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.mode = GameMode.Easy;
    this.setup();
  }

  ngAfterViewInit(): void {
    this.startPopup.nativeElement.classList.add(this._classOpen);
  }

  onRestart(): void {
    this.setup();
  }

  onPopupClose(isRestartRequired: boolean = false): void {
    this.resultPopup.nativeElement.classList.remove(this._classOpen);
    this.startPopup.nativeElement.classList.remove(this._classOpen);

    if (isRestartRequired) {
      this.setup();
    }
  }

  onCellClick(item: MultiplicationItem, cell: EventTarget): void {
    const isCorrectAnswer =
      item.product === this._questionsTable[this._currentStep].product;
    const isGameOver = this._currentStep >= this._questionsTable.length - 1;

    if (!isCorrectAnswer) {
      this._correctAnswersCount--;
      return;
    }

    (cell as HTMLElement).classList.add("invisible");
    this._correctAnswersCount++;

    if (isGameOver) {
      this.currentQuestion$.next("МОЛОДЕЦ!");

      setTimeout(() => {
        this.resultPopup.nativeElement.classList.add(this._classOpen);
        this.resultModel = new ResultModel(
          this._correctAnswersCount,
          this._questionsTable.length
        );
        this.cdref.detectChanges();
      }, 350);
      return;
    }

    this._currentStep++;
    this.renderQuestion(this._currentStep);
  }

  private setup(): void {
    this._currentStep = 0;
    this._correctAnswersCount = 0;
    this.currentQuestion$.next("");
    this._multiplicationTable = this.getMultiplicationTable();
    this._questionsTable = this.getQuestionsTable();

    this.renderQuestion();
    this.answerBoard$.next({
      image: this.getRandom(1, this._maxImages).toString(), // setup random cartoon image
      table: this._multiplicationTable,
    });
  }

  private getMultiplicationTable(): Array<MultiplicationItem[]> {
    const result = [];
    const rowShuffledTable = this.getFilledTable().map(
      (it: MultiplicationItem[]) => ArrayUtil.shuffle(it)
    );

    const fullyShuffledArray = ArrayUtil.shuffle(rowShuffledTable);
    const [rowsRange, columnsRange] = this._gameBoard[this.mode];

    for (let i = 0; i < rowsRange; i++) {
      const tableRow: MultiplicationItem[] = [];

      for (let j = 0; j < columnsRange; j++) {
        tableRow.push(fullyShuffledArray[i][j]);
      }

      result.push(tableRow);
    }

    return result;
  }

  private getQuestionsTable(): MultiplicationItem[] {
    const shuffledByRow: MultiplicationItem[] = ArrayUtil.flatten(
      this._multiplicationTable.map((it: MultiplicationItem[]) =>
        ArrayUtil.shuffle(it)
      )
    );

    return ArrayUtil.shuffle(shuffledByRow);
  }

  private getFilledTable(): Array<MultiplicationItem[]> {
    const result = [];

    for (let i = 2; i <= 9; i++) {
      const tableRow: MultiplicationItem[] = [];

      for (let j = 1; j <= 10; j++) {
        tableRow.push(new MultiplicationItem(i, j));
      }

      result.push(tableRow);
    }

    return result;
  }

  private renderQuestion(index: number = 0): void {
    this.currentQuestion$.next(this._questionsTable[index].toString());
  }

  private getRandom(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
