export class ArrayUtil {
  public static shuffle<T>(array: T[]): T[] {
    const result: T[] = Object.create(array);

    for (let i = result.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [result[i], result[j]] = [result[j], result[i]];
    }

    return result;
  }

  public static flatten<T>(array: T[][]): T[] {
    const result: T[] = [];
    array.forEach(it => it.forEach(i => result.push(i)));

    return result;
  }
}
