interface IRating {
  [percent: number]: string[];
}

export interface IResultStat {
  percentage: number;
  title: string;
  emoji: string;
}

export class ResultStat {
  private static rating: IRating = {
    85: ["Отлично!", "&#x1F603;"],
    60: ["Хорошо!", "&#x1F610;"],
    50: ["Неплохо!", "&#x1F615;"],
  };

  public static getResultStat(
    correctAnswers: number,
    totalAnswers: number
  ): IResultStat {
    const percentage = correctAnswers / (totalAnswers / 100);

    const percents = Object.keys(this.rating)
      .sort((a: string, b: string) => {
        return a > b ? -1 : 1;
      })
      .map((it) => Number(it));

    let result: string[] = [];

    for (const percent of percents) {
      if (percentage % percent === percentage) {
        continue;
      }

      result = this.rating[percent];
      break;
    }

    const [title, emoji] = result;

    return {
      percentage,
      title,
      emoji,
    };
  }
}
